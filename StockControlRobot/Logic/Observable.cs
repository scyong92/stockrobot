﻿using System.Collections.Generic;
using System.Threading;

namespace StockControlRobot.Logic
{
    public class Observable
    {
        private List<IObserver> _observers = new List<IObserver>();

        public virtual void Attach(IObserver observer)
        {
            _observers.Add(observer);
        }

        public virtual void Detach(IObserver observer)
        {
            _observers.Remove(observer);
        }

        public virtual void StateChange(object obj)
        {
            foreach (var ea in _observers)
            {
                ea.Update(this, obj);
            }
        }
    }
}
