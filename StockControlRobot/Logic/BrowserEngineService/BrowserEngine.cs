﻿using PuppeteerSharp;
using StockControlRobot.Common;
using StockControlRobot.Database;
using StockControlRobot.Logic.Stock;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace StockControlRobot.Logic.BrowserEngineService
{
    public class BrowserEngine
    {
        private static BrowserEngine _instance;
        private bool _isInitialized = false;
        private List<Engine> _engineList = new List<Engine>();

        [MethodImpl(MethodImplOptions.Synchronized)]
        public static BrowserEngine GetInstance()
        {
            if (_instance == null)
            {
                _instance = new BrowserEngine();
            }

            return _instance;
        }

        public async Task InitializeEngine(List<StockModel> stockList)
        {
            try
            {
                StockObservable.GetInstance().StateChange(new StockEvent(null, StockEventEnum.STOCK_INITIALIZATION, true));
                if (!_isInitialized)
                {
                    Util.WriteLog("Start Initializing, downloading browser");

                    await new BrowserFetcher().DownloadAsync(BrowserFetcher.DefaultRevision);

                    Util.WriteLog("Downloaded Chromium");

                    for (int j = 0; j < stockList.Count(); j++)
                    {
                        StockModel stockDetail = stockList[j];

                        Browser browser = await Puppeteer.LaunchAsync(new LaunchOptions { Headless = false });
                        Engine engine = new Engine(browser, stockDetail);
                        stockDetail.Status = StockModel.STOCK_ONLINE_STATUS;
                        _engineList.Add(engine);
                        await engine.Init();
                    }
                }

                _isInitialized = true;
            }
            catch (Exception ex)
            {
                await Shutdown();
                throw ex;
            }
            finally
            {
                StockObservable.GetInstance().StateChange(new StockEvent(null, StockEventEnum.STOCK_INITIALIZATION, false));
            }
        }

        public async Task Shutdown()
        {
            _isInitialized = false;
            foreach (var engine in _engineList)
            {
                engine.Abort();
                await engine.Cleanup();
            }

            _engineList.Clear();
        }

        public void StartLearn()
        {
            foreach (var engine in _engineList)
            {
                engine.StartLearn();
            }
        }

        public void StopLearn()
        {
            foreach (var engine in _engineList)
            {
                engine.Abort();
            }
        }

        public bool IsInitialized()
        {
            return _isInitialized;
        }
    }
}
