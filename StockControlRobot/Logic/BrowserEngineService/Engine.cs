﻿using PuppeteerSharp;
using PuppeteerSharp.Input;
using StockControlRobot.Database;
using StockControlRobot.Logic.Stock;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace StockControlRobot.Logic.BrowserEngineService
{
    public class Engine
    {
        private bool _aborted = false;
        private Page _page;
        private Browser _browser;
        private bool _isBusy = false;
        private StockModel _stock;
        private const int STOCK_NAME_INDEX = 0;
        private const int BUY_PRICE_INDEX = 5;
        private const int SELL_PRICE_INDEX = 6;

        private const int WAIT_PAGE_MEDIUM_TIMEOUT_IN_MS = 3000;
        private const int WAIT_PAGE_LONG_PROCESS_IN_MS = 10000;
        private const int WAIT_PAGE_LOAD_PREPARE_IN_MS = 500;

        private int _dataReadInterval;
        private const string GET_INNER_TEXT_READING_QUERY = "node => node.innerText";
        private const string STOCK_FRAME_NAME = "ifrmPriceFeed";
        private const string LOGIN_FRAME_URL = "https://trade.kentrade.com.my/kentrade/loginRSA.aspx";
        private ElementHandle _stockRowHandle;
        private Thread _monitorThread;
        private StockEngine _stockEngine;
        private int _stockReferenceId = 0;

        public Engine( Browser browser, StockModel stock)
        {
            _browser = browser;
            _stock = stock;
            _dataReadInterval = Config.GetInstance().GetIntegerValue(ConfigEnum.DATA_COLLECT_INTERVAL_IN_MS);
            _stockEngine = new StockEngine(stock);
        }

        public bool IsAbort()
        {
            return _aborted;
        }

        public void Abort()
        {
            _aborted = true;
        }

        public bool IsBusy()
        {
            return _isBusy;
        }

        public async Task Cleanup()
        {
            Abort();

            if (await IsLoggedIn(WAIT_PAGE_LOAD_PREPARE_IN_MS))
            {
                await _page.ClickAsync("#ctl00_ibSignOut");
            }

            await _page.CloseAsync();
            _page.Dispose();

            await _page.Browser.CloseAsync();
            _page.Browser.Dispose();
        }

        public async Task Init()
        {
            try
            {
                string website = Config.GetInstance().GetStringValue(ConfigEnum.WEBSITE);
                _page = (await _browser.PagesAsync())[0];
                await _page.GoToAsync(website, new NavigationOptions { WaitUntil = new WaitUntilNavigation[] { WaitUntilNavigation.Networkidle0 } });
                Frame loginFrame = _page.Frames.FirstOrDefault(i => i.Url == LOGIN_FRAME_URL);
                await loginFrame.WaitForSelectorAsync("#ctl00_cntPlcHldrContent_txtUsrID", new WaitForSelectorOptions { Visible = true, Timeout = WAIT_PAGE_MEDIUM_TIMEOUT_IN_MS });
                await loginFrame.ClickAsync("#ctl00_cntPlcHldrContent_txtUsrID", new ClickOptions { ClickCount = 2 });
                await loginFrame.TypeAsync("#ctl00_cntPlcHldrContent_txtUsrID", _stock.LoginUser);

                await loginFrame.WaitForSelectorAsync("#ctl00_cntPlcHldrContent_txtUsrPwd", new WaitForSelectorOptions { Visible = true, Timeout = WAIT_PAGE_MEDIUM_TIMEOUT_IN_MS });
                await loginFrame.ClickAsync("#ctl00_cntPlcHldrContent_txtUsrPwd", new ClickOptions { ClickCount = 2});
                await loginFrame.TypeAsync("#ctl00_cntPlcHldrContent_txtUsrPwd", _stock.LoginUserPassword);

                loginFrame.ClickAsync("#ctl00_cntPlcHldrContent_btnSign");

                await _page.WaitForNavigationAsync(new NavigationOptions { WaitUntil = new WaitUntilNavigation[] { WaitUntilNavigation.Networkidle0 } });
                if (await IsLoggedIn(WAIT_PAGE_LONG_PROCESS_IN_MS))
                {
                    Frame stockFrame = _page.Frames.FirstOrDefault(i => i.Name == STOCK_FRAME_NAME);

                    await stockFrame.ClickAsync("#ctl00_cntPlcHldrContent_lblPageType");
                    await stockFrame.WaitForSelectorAsync("#ulPageType", new WaitForSelectorOptions { Visible = true, Timeout = WAIT_PAGE_MEDIUM_TIMEOUT_IN_MS });
                    ElementHandle[] favoriteList = await stockFrame.QuerySelectorAllAsync("#ulPageType li");

                    bool favoriteFound = false;
                    foreach (var ea in favoriteList)
                    {
                        string stockInView = await ea.EvaluateFunctionAsync<string>(GET_INNER_TEXT_READING_QUERY);
                        if (_stock.FavoriteName == stockInView)
                        {
                            await ea.ClickAsync();
                            await stockFrame.WaitForTimeoutAsync(WAIT_PAGE_LOAD_PREPARE_IN_MS);
                            favoriteFound = true;
                            break;
                        }
                    }

                    if (!favoriteFound)
                    {
                        throw new Exception("Favorite " + _stock.FavoriteName + " is not found");
                    }

                    await stockFrame.WaitForSelectorAsync("#tblStkList", new WaitForSelectorOptions { Visible = true, Timeout = WAIT_PAGE_MEDIUM_TIMEOUT_IN_MS });

                    ElementHandle[] stockList = await stockFrame.QuerySelectorAllAsync("#tblStkList tbody tr");

                    foreach (var ea in stockList)
                    {
                        ElementHandle[] columnHandle = await ea.QuerySelectorAllAsync("td");
                        string stockInView = await columnHandle[STOCK_NAME_INDEX].EvaluateFunctionAsync<string>(GET_INNER_TEXT_READING_QUERY);
                        if (stockInView == _stock.StockName)
                        {
                            _stockRowHandle = ea;
                            break;
                        }
                    }

                    if (_stockRowHandle == null)
                    {
                        throw new Exception("Stock "+_stock.StockName +" is not found");
                    }
                }
                else
                {
                    string errorMessage = "";
                    loginFrame = _page.Frames.FirstOrDefault(i => i.Url == LOGIN_FRAME_URL);
                    try
                    {
                        errorMessage = await loginFrame.QuerySelectorAsync("#ctl00_cntPlcHldrContent_lblErrMsg").EvaluateFunctionAsync<string>(GET_INNER_TEXT_READING_QUERY);
                    }
                    catch(Exception ex)
                    {
                        throw ex;
                    }

                    throw new Exception(errorMessage);
                }
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }

        public void StartLearn()
        {
            _aborted = false;
            _monitorThread = new Thread(MonitorStock);
            _monitorThread.Start();
        }

        private async void MonitorStock()
        {
            try
            {
                _stockReferenceId = _stockEngine.CreateStockDataWithDate();
                _stock.Status = StockModel.STOCK_MONITORING_STATUS;

                StockObservable.GetInstance().StateChange(new StockEvent(_stock, StockEventEnum.STOCK_LEARNING, true));

                decimal lastValue = _stockEngine.GetLatestBuyValue(_stockReferenceId);
                while (!_aborted)
                {
                    ElementHandle[] columnHandle = await _stockRowHandle.QuerySelectorAllAsync("td");
                    decimal buyValue = decimal.Parse(await columnHandle[BUY_PRICE_INDEX].EvaluateFunctionAsync<string>(GET_INNER_TEXT_READING_QUERY));

                    if (lastValue != buyValue)
                    {
                        _stockEngine.RecordData(buyValue, _stockReferenceId);
                        lastValue = buyValue;
                    }

                    Thread.Sleep(_dataReadInterval);
                }
            }
            catch(Exception ex)
            {
                throw ex;
            }
            finally
            {
                _stock.Status = StockModel.STOCK_ONLINE_STATUS;
                StockObservable.GetInstance().StateChange(new StockEvent(_stock, StockEventEnum.STOCK_LEARNING, false));
            }
        }

        private async Task<bool> IsLoggedIn(int timeout = WAIT_PAGE_LOAD_PREPARE_IN_MS)
        {
            try
            {
                await _page.WaitForSelectorAsync("#ctl00_ibSignOut", new WaitForSelectorOptions { Visible = true, Timeout = timeout });
                return true;
            }
            catch(Exception ex)
            {
                return false;
            }
        }
    }
}
