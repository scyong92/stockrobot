﻿using System;

namespace StockControlRobot.Logic
{
    public interface IObserver
    {
        void Update(Observable observable, Object obj);
    }
}
