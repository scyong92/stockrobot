﻿using StockControlRobot.Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StockControlRobot.Logic.Stock
{
    public class StockEngine
    {
        private StockModel _stock;

        public StockEngine(StockModel stock)
        {
            _stock = stock;
        }

        public void RecordData(decimal value, int stockReferenceId)
        {
            string newStockRecord = @"INSERT INTO StockData (StockId, BuyValue) 
                                      VALUES (" + stockReferenceId + ", " + value +")";
            DataContext.GetInstance().ExecuteNonQuery(newStockRecord);
        }

        public decimal GetLatestBuyValue(int stockReferenceId)
        {
            string lastRecordValue = "select BuyValue from 'StockData' where StockId = "+ stockReferenceId + " order by CreatedDate desc LIMIT 1";
            object number = DataContext.GetInstance().ExecuteScalarQuery(lastRecordValue);
            return number != null ? Convert.ToDecimal(number) : 0m;
        }

        public int CreateStockDataWithDate()
        {
            int referenceRecordId = 0;
            try
            {
                string newStockRecord = @"INSERT INTO Stock (Stockcode, StepSize, Interval) 
                                      VALUES ('" + _stock.StockName + "', " + _stock.SteppingSize + "," + _stock.IntervalInSecond + ")";

                newStockRecord += "; select max(Id) from Stock where StockCode = '"+ _stock.StockName+ "' and CreatedDate='"+DateTime.Now.ToString("yyyy-MM-dd")+"'";
                referenceRecordId = Convert.ToInt32(DataContext.GetInstance().ExecuteScalarQuery(newStockRecord));
            }
            catch(Exception ex)
            {
                referenceRecordId = Convert.ToInt32(DataContext.GetInstance().ExecuteScalarQuery("select max(Id) from Stock where StockCode = '" + _stock.StockName + "' and CreatedDate='" + DateTime.Now.ToString("yyyy-MM-dd") + "'"));
            }
            return referenceRecordId;
        }
    }
}
