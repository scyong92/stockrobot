﻿namespace StockControlRobot.Logic.Stock
{
    public class StockEvent
    {
        public object Source { get; }
        public StockEventEnum EventEnum { get; }
        public bool IsStart { get; }

        public StockEvent(object source, StockEventEnum stockEvent, bool isStart)
        {
            IsStart = isStart;
            Source = source;
            EventEnum = stockEvent;
        }
    }
}
