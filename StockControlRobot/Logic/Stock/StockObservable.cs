﻿using System.Runtime.CompilerServices;

namespace StockControlRobot.Logic.Stock
{
    public class StockObservable : Observable
    {
        private static StockObservable _instance;

        [MethodImpl(MethodImplOptions.Synchronized)]
        public static StockObservable GetInstance()
        {
            if (_instance == null)
            {
                _instance = new StockObservable();
            }
            return _instance;
        }

        public void StateChange(StockEvent stockEvenum)
        {
            base.StateChange(stockEvenum);
        }
    }
}
