﻿namespace StockControlRobot.Logic.Stock
{
    public class StockEventEnum : Common.Enum
    {
        private static int _index = -1;
        public static StockEventEnum STOCK_INITIALIZATION = new StockEventEnum(++_index);
        public static StockEventEnum STOCK_LEARNING = new StockEventEnum(++_index);

        public StockEventEnum(int index):
            base(index)
        {

        }
    }
}
