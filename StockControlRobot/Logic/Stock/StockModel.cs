﻿using System;

namespace StockControlRobot.Logic.Stock
{
    public class StockModel
    {
        public const int STOCK_MONITORING_STATUS = 2;
        public const int STOCK_ONLINE_STATUS = 1;
        public const int STOCK_OFFLINE_STATUS = 0;

        public const int DEFAULT_STEPPING_SIZE = 2;
        public const int DEFAULT_INTERVAL_IN_SECOND = 300;

        public String StatusName
        {
            get
            {
                if (this.Status == STOCK_ONLINE_STATUS)
                {
                    return "Online";
                }
                else if (this.Status == STOCK_MONITORING_STATUS)
                {
                    return "Monitoring";
                }
                else
                {
                    return "Offline";
                }
            }
        }

        public String LoginUser { get; set; }
        public String LoginUserPassword { get; set; }
        public String StockName { get; set; }
        public String FavoriteName { get; set; }
        public int SteppingSize { get; set; } = DEFAULT_STEPPING_SIZE;
        public int IntervalInSecond { get; set; } = DEFAULT_INTERVAL_IN_SECOND;
        public int Status { get; set; }
    }
}
