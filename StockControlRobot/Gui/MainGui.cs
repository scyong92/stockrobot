﻿using StockControlRobot.Common;
using StockControlRobot.Database;
using StockControlRobot.Gui.Configuration;
using StockControlRobot.Gui.Production;
using StockControlRobot.Logic.BrowserEngineService;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace StockControlRobot.Gui
{
    public class MainGui : Form
    {
        private static MainGui _instance;
        private MenuStrip _topMenu = new MenuStrip();
        private ToolStripMenuItem _fileMenuItem = new ToolStripMenuItem();
        private ToolStripMenuItem _settingMenuItem = new ToolStripMenuItem();

        private ToolStripMenuItem _productionMenuItem = new ToolStripMenuItem();
        private ToolStripMenuItem _stockControlMenuItem = new ToolStripMenuItem();

        private IPanel _settingPanel = new SettingPanel();
        private IPanel _stockMonitoringPanel = new StockMonitoringPanel();

        private Panel _topPanel = new Panel();
        private IPanel _currentPanel;

        [MethodImpl(MethodImplOptions.Synchronized)]
        public static MainGui GetInstance()
        {
            if (_instance == null)
            {
                _instance = new MainGui();
            }

            return _instance;
        }

        public MainGui()
        {
            Init();
        }

        private void Init()
        {
            _topPanel.Dock = DockStyle.Top;
            _topPanel.AutoSize = true;
            _topPanel.AutoSizeMode = AutoSizeMode.GrowAndShrink;

            _settingMenuItem.Text = "Setting";

            _fileMenuItem.Text = "File";
            _fileMenuItem.DropDownItems.AddRange(new ToolStripItem[] {_settingMenuItem });
            _settingMenuItem.Click += new EventHandler((sender, e) => MenuClickEvent(sender, e, (IPanel)_settingPanel));

            _stockControlMenuItem.Text = "Stock Control";
            _productionMenuItem.Text = "Production";
            _productionMenuItem.DropDownItems.AddRange(new ToolStripItem[] { _stockControlMenuItem });
            _stockControlMenuItem.Click += new EventHandler((sender, e) => MenuClickEvent(sender, e, (IPanel)_stockMonitoringPanel));

            _topMenu.Items.AddRange(new ToolStripItem[] { _fileMenuItem, _productionMenuItem });
            _topPanel.Controls.Add(_topMenu);
            _topPanel.BorderStyle = BorderStyle.FixedSingle;

            Controls.Add((Panel)_settingPanel);
            Controls.Add((Panel)_stockMonitoringPanel);
            Controls.Add(_topPanel);

            _currentPanel = _settingPanel;

            Size = new Size(860, 640);
            SwitchScreen(_stockMonitoringPanel);
        }

        private void MenuClickEvent(Object sender, EventArgs e, IPanel panel)
        {
            SwitchScreen(panel);
        }

        public void SwitchScreen(IPanel screen)
        {
            if (_currentPanel != null)
            {
                IPanel currentPanel = (IPanel)_currentPanel;

                if (!currentPanel.IsReadyToClose())
                {
                    return;
                }
                currentPanel.Stop();
                ((Panel)currentPanel).Hide();
            }
            _currentPanel = screen;

            screen.Start();
            ((Panel)screen).Show();
        }

        public void HandleException(Exception exception)
        {
            Util.WriteLog(exception.Message);
            MessageBox.Show(exception.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        protected override void OnClosing(CancelEventArgs e)
        {
            Task.Run(async () =>
            {
                await BrowserEngine.GetInstance().Shutdown();
            }).Wait();

            base.OnClosing(e);
        }

        protected override void OnLoad(EventArgs e)
        {
            DataContext.GetInstance().Init();

            base.OnLoad(e);
        }
    }
}
