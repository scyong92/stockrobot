﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace StockControlRobot.Gui
{
    public class BusyDialog : Form
    {
        private ProgressBar _loadingProgressBar = new ProgressBar();
        private FlowLayoutPanel _mainPanel = new FlowLayoutPanel();
        private Label _pleaseWaitLabel = new Label();
        private bool _prepareToClose = false;

        public BusyDialog(string message = "")
        {
            Init(message);
        }

        private void Init(string message)
        {
            FormClosing += FormClosingAction;
            ControlBox = false;
            _mainPanel.FlowDirection = FlowDirection.TopDown;
            _mainPanel.AutoSize = true;
            _mainPanel.AutoSizeMode = AutoSizeMode.GrowAndShrink;
            _loadingProgressBar.Style = ProgressBarStyle.Marquee;
            _loadingProgressBar.Width = 200;
            _loadingProgressBar.Height = 30;

            AutoSize = true;
            AutoSizeMode = AutoSizeMode.GrowAndShrink;

            _pleaseWaitLabel.Text = string.IsNullOrEmpty(message) ? "Please Wait" : message;
            _pleaseWaitLabel.Dock = DockStyle.Fill;
            _pleaseWaitLabel.Padding = new Padding(0,0,0,10);
            _pleaseWaitLabel.TextAlign = ContentAlignment.MiddleCenter;
            _mainPanel.Padding = new Padding(10);

            _mainPanel.Controls.Add(_pleaseWaitLabel);
            _mainPanel.Controls.Add(_loadingProgressBar);

            Controls.Add(_mainPanel);
        }

        private void FormClosingAction(Object sender, CancelEventArgs e)
        {
            e.Cancel = !_prepareToClose;
        }

        public void CloseForm()
        {
            _prepareToClose = true;
            Close();
        }
    }
}
