﻿namespace StockControlRobot.Gui
{
    public interface IPanel
    {
        bool IsReadyToClose();
        void Stop();
        void Start();
    }
}
