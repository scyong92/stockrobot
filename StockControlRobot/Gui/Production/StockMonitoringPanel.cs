﻿using StockControlRobot.Gui.Configuration;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

namespace StockControlRobot.Gui.Production
{
    public class StockMonitoringPanel : Panel, IPanel
    {
        private MenuStrip _sideMenu = new MenuStrip();
        private ToolStripMenuItem _LearnItem = new ToolStripMenuItem();
        private ToolStripMenuItem _ProductionItem = new ToolStripMenuItem();
        private IPanel _dataLearnPanel = new DataLearnPanel();
        private IPanel _liveAnalysisPanel = new LiveAnalysisPanel();
        private IPanel _currentPanel;

        private Dictionary<IPanel, ToolStripMenuItem> _panelToMenuDic = new Dictionary<IPanel, ToolStripMenuItem>();

        public StockMonitoringPanel()
        {
            _panelToMenuDic.Add(_dataLearnPanel, _LearnItem);
            _panelToMenuDic.Add(_liveAnalysisPanel, _ProductionItem);
            Init();
        }

        private void Init()
        {
            Dock = DockStyle.Fill;

            _LearnItem.Text = "Learn";
            _LearnItem.Padding = new Padding(5, 15, 5, 15);
            _LearnItem.Click += new EventHandler((sender, e) => MenuClickEvent(sender, e, _dataLearnPanel));
            _LearnItem.Dock = DockStyle.Fill;

            _ProductionItem.Text = "Production";
            _ProductionItem.Padding = new Padding(5, 15, 5, 15);
            _ProductionItem.Click += new EventHandler((sender, e) => MenuClickEvent(sender, e, _liveAnalysisPanel));
            _ProductionItem.Dock = DockStyle.Fill;

            _sideMenu.Items.Add(_LearnItem);
            _sideMenu.Items.Add(_ProductionItem);
            _sideMenu.Dock = DockStyle.Left;

            _sideMenu.Renderer = new TopMenuRenderer();
            Controls.Add((Panel)_dataLearnPanel);
            Controls.Add((Panel)_liveAnalysisPanel);
            Controls.Add(_sideMenu);
            _currentPanel = _dataLearnPanel;
        }

        public bool IsReadyToClose()
        {
            return ((IPanel)_currentPanel).IsReadyToClose();
        }

        public void Start()
        {
            SwitchScreen(_currentPanel);
        }

        public void Stop()
        {
            ((IPanel)_currentPanel).Stop();
        }

        private void MenuClickEvent(Object sender, EventArgs e, IPanel panel)
        {
            SwitchScreen(panel);
        }

        public void SwitchScreen(IPanel screen)
        {
            if (_currentPanel != null)
            {
                IPanel currentPanel = (IPanel)_currentPanel;

                if (!currentPanel.IsReadyToClose())
                {
                    return;
                }

                currentPanel.Stop();
                ((Panel)currentPanel).Hide();
                _panelToMenuDic[currentPanel].Enabled = true;
            }

            _currentPanel = screen;

            screen.Start();
            ((Panel)screen).Show();
            _panelToMenuDic[screen].Enabled = false;
        }
    }
}
