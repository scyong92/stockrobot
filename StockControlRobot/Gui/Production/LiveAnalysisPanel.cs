﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace StockControlRobot.Gui.Production
{
    public class LiveAnalysisPanel : Panel, IPanel
    {
        public LiveAnalysisPanel()
        {
            Init();
        }

        private void Init()
        {
            BackColor = Color.White;
            Dock = DockStyle.Fill;
        }

        public bool IsReadyToClose()
        {
            return true;
        }

        public void Start()
        {

        }

        public void Stop()
        {

        }
    }
}
