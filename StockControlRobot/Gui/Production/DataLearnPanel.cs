﻿using StockControlRobot.Database;
using StockControlRobot.Logic;
using StockControlRobot.Logic.BrowserEngineService;
using StockControlRobot.Logic.Stock;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace StockControlRobot.Gui.Production
{
    public class DataLearnPanel : Panel, IPanel , IObserver
    {
        private GroupBox _settingGroupBox = new GroupBox();
        private GroupBox _advanceGroupBox = new GroupBox();
        private FlowLayoutPanel _mainPanel = new FlowLayoutPanel();
        private FlowLayoutPanel _advanceSettingPanel = new FlowLayoutPanel();
        private CheckBox _useOnlyNewData = new CheckBox();
        private Button _startBtn = new Button();
        private Button _stopBtn = new Button();
        private FlowLayoutPanel _actionPanel = new FlowLayoutPanel();
        private TableLayoutPanel _stockPanel = new TableLayoutPanel();
        private Dictionary<StockModel, KeyValuePair<Label, Label>> _stockNameToDataDic = new Dictionary<StockModel, KeyValuePair<Label, Label>>();
        private BackgroundWorker _worker = new BackgroundWorker();
        private BusyDialog _learnBusyDialog = new BusyDialog();
        private bool _isbusy = false;

        public DataLearnPanel()
        {
            Init();
        }

        private void Init()
        {
            BackColor = Color.White;
            Dock = DockStyle.Fill;

            _useOnlyNewData.Text = "Use Only New Data For Learning";
            _useOnlyNewData.TextAlign = ContentAlignment.MiddleCenter;
            _useOnlyNewData.AutoSize = true;

            _mainPanel.Dock = DockStyle.Fill;
            _mainPanel.Padding = new Padding(10, 10, 10, 10);
            _mainPanel.FlowDirection = FlowDirection.TopDown;

            _advanceGroupBox.Text = "Stock Information";
            _advanceGroupBox.AutoSizeMode = AutoSizeMode.GrowAndShrink;
            _advanceGroupBox.AutoSize = true;
            _advanceGroupBox.AutoSizeMode = AutoSizeMode.GrowOnly;
            _advanceGroupBox.Controls.Add(_advanceSettingPanel);
            _advanceGroupBox.Margin = new Padding(0, 20, 0, 20);

            _advanceSettingPanel.Location = new Point(5, 20);
            _advanceSettingPanel.AutoSize = true;
            _advanceSettingPanel.AutoSizeMode = AutoSizeMode.GrowAndShrink;
            _advanceSettingPanel.FlowDirection = FlowDirection.TopDown;
            _advanceSettingPanel.Controls.Add(_stockPanel);
            _stockPanel.AutoSize = true;
            _stockPanel.AutoSizeMode = AutoSizeMode.GrowOnly;

            _startBtn.Text = "Start";
            _startBtn.Click += StartBtnAction;
            _stopBtn.Text = "Stop";
            _stopBtn.Click += StopBtnAction;

            _actionPanel.FlowDirection = FlowDirection.LeftToRight;
            _actionPanel.AutoSize = true;
            _actionPanel.Controls.Add(_startBtn);
            _actionPanel.Controls.Add(_stopBtn);

            _stopBtn.Enabled = false;

            _mainPanel.Controls.Add(_useOnlyNewData);
            _mainPanel.Controls.Add(_advanceGroupBox);
            _mainPanel.Controls.Add(_actionPanel);

            Controls.Add(_mainPanel);
        }

        public bool IsReadyToClose()
        {
            return !_isbusy;
        }

        public void Start()
        {
            StockObservable.GetInstance().Attach(this);
            ReadData();
        }

        public void Stop()
        {
            StockObservable.GetInstance().Detach(this);
            PerformStop();
        }

        private void StartBtnAction(object sender, EventArgs e)
        {
            _startBtn.Enabled = false;
            _stopBtn.Enabled = true;
            _useOnlyNewData.Enabled = false;
            _isbusy = true;

            _worker = new BackgroundWorker();
            _worker.DoWork += StartLearn;
            _worker.WorkerSupportsCancellation = true;
            _worker.RunWorkerCompleted += StartLearnJobFinish;
            _worker.RunWorkerAsync(true);

            _learnBusyDialog.StartPosition = FormStartPosition.CenterParent;
            _learnBusyDialog.ShowDialog(this);
        }

        private void StartLearnJobFinish(object sender, RunWorkerCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                PerformStop();
                MainGui.GetInstance().HandleException(e.Error.InnerException);
            }
            _learnBusyDialog.CloseForm();
        }

        public void StartLearn(object sender, DoWorkEventArgs e)
        {
            BrowserEngine.GetInstance().InitializeEngine(_stockNameToDataDic.Keys.ToList()).Wait();
            BrowserEngine.GetInstance().StartLearn();
        }

        private void StopBtnAction(object sender, EventArgs e)
        {
            _worker.CancelAsync();
            PerformStop();
        }

        private void PerformStop()
        {
            _startBtn.Enabled = true;
            _stopBtn.Enabled = false;
            _useOnlyNewData.Enabled = true;
            _isbusy = false;
            BrowserEngine.GetInstance().StopLearn();

            UpdateStockStatus();
        }

        private void ReadData()
        {
            List<string> stockList = Config.GetInstance().GetStringValue(ConfigEnum.STOCK_TO_VIEW).Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries).ToList();
            bool hasStockConfigured = stockList.Count() != 0;

            _stockPanel.Controls.Clear();
            _stockPanel.ColumnCount = 3;

            _stockPanel.ColumnStyles.Add(new ColumnStyle(SizeType.AutoSize));
            _stockPanel.ColumnStyles.Add(new ColumnStyle(SizeType.AutoSize));
            _stockPanel.ColumnStyles.Add(new ColumnStyle(SizeType.AutoSize));
            _stockPanel.RowCount = stockList.Count();

            _stockNameToDataDic.Clear();
            _startBtn.Enabled = hasStockConfigured;
            _stockPanel.Visible = hasStockConfigured;

            for (int i = 0; i < stockList.Count;i++)
            {
                string[] stockData = stockList[i].Split(Config.FIELD_SEPARATOR);
                StockModel stock = new StockModel();
                stock.LoginUser = stockData[0];
                stock.LoginUserPassword = stockData[1];
                stock.FavoriteName = stockData[2];
                stock.StockName = stockData[3];
                stock.Status = StockModel.STOCK_OFFLINE_STATUS;
                stock.IntervalInSecond = int.Parse(stockData[4]);
                stock.SteppingSize = int.Parse(stockData[5]);

                _stockPanel.RowStyles.Add(new RowStyle(SizeType.AutoSize));
                Label stockNameLabel = new Label();
                stockNameLabel.Text = stock.StockName;

                Label stockStatuslabel = new Label();
                Label stockSettingLabel = new Label();

                _stockPanel.Controls.Add(stockNameLabel, 0, i);
                _stockPanel.Controls.Add(stockStatuslabel, 1, i);
                _stockPanel.Controls.Add(stockSettingLabel, 2, i);

                _stockNameToDataDic.Add(stock, new KeyValuePair<Label, Label>(stockStatuslabel, stockSettingLabel));

                UpdateIndividualStockInfo((KeyValuePair<Label, Label>)_stockNameToDataDic[stock], stock);
            }

            if (!hasStockConfigured)
            {
                MainGui.GetInstance().HandleException(new Exception("No Stock Configured, Please setup first"));
            }
        }

        public void Update(Observable observable, object obj)
        {
            BeginInvoke((Action)(() => 
            {
                if (observable is StockObservable)
                {
                    StockEvent stockEvent = (StockEvent)obj;
                    if (stockEvent.EventEnum.Equals(StockEventEnum.STOCK_INITIALIZATION) && !stockEvent.IsStart)
                    {
                        UpdateStockStatus();
                    }
                    else if (stockEvent.EventEnum.Equals(StockEventEnum.STOCK_LEARNING))
                    {
                        StockModel _stockModelReceived = (StockModel)stockEvent.Source;

                        UpdateIndividualStockInfo((KeyValuePair<Label, Label>)_stockNameToDataDic[_stockModelReceived], _stockModelReceived);
                    }
                }
            }));
        }

        private void UpdateStockStatus()
        {
            foreach (var ea in _stockNameToDataDic)
            {
                UpdateIndividualStockInfo((KeyValuePair<Label, Label>)ea.Value, ea.Key);
            }
        }

        private void UpdateIndividualStockInfo(KeyValuePair<Label, Label> displayLabel, StockModel model)
        {
            displayLabel.Key.Text = model.StatusName;
            displayLabel.Key.ForeColor = model.Status == StockModel.STOCK_OFFLINE_STATUS ? Color.Red : Color.Green;
            displayLabel.Value.Text = model.SteppingSize.ToString() + " / " + model.IntervalInSecond.ToString();
        }
    }
}
