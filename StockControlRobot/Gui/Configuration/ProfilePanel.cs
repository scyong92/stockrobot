﻿using StockControlRobot.Common;
using StockControlRobot.Database;
using StockControlRobot.Logic.Stock;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;

namespace StockControlRobot.Gui.Configuration
{
    public class ProfilePanel : Panel, IPanel
    {
        private GroupBox _stockGroupBox = new GroupBox();
        private DataGridView _stockGridView = new DataGridView();
        private FlowLayoutPanel _controlPanel = new FlowLayoutPanel();
        private Button _saveBtn = new Button();

        public ProfilePanel()
        {
            Init();
        }

        private void Init()
        {
            BackColor = Color.White;
            Dock = DockStyle.Fill;

            _stockGroupBox.Padding = new Padding(20);
            _stockGroupBox.Dock = DockStyle.Fill;
            _stockGroupBox.Text = "Monitoring Stock";
            _stockGroupBox.Controls.Add(_stockGridView);

            _stockGridView.BorderStyle = BorderStyle.None;
            _stockGridView.Dock = DockStyle.Fill;
            _stockGridView.ColumnCount = 6;
            _stockGridView.Columns[0].HeaderCell.Value = "Username";
            _stockGridView.Columns[1].HeaderCell.Value = "Password";
            _stockGridView.Columns[2].HeaderCell.Value = "Associated Favorite List";
            _stockGridView.Columns[3].HeaderCell.Value = "Stock Code";
            _stockGridView.Columns[4].HeaderCell.Value = "Interval";
            _stockGridView.Columns[5].HeaderCell.Value = "Step Size";
            _stockGridView.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;

            _stockGridView.Location = new Point(10, 25);

            _controlPanel.Dock = DockStyle.Bottom;
            _controlPanel.Padding =  new Padding(10);

            _saveBtn.Text = "Save Setting";
            _saveBtn.Click += SaveBtnAction;
            _controlPanel.Controls.Add(_saveBtn);
            
            Padding = new Padding(20);
            Controls.Add(_stockGroupBox);
            Controls.Add(_controlPanel);
        }

        public bool IsReadyToClose()
        {
            return true;
        }

        public void Start()
        {
            ReadData();
        }

        public void Stop()
        {
            //do nothing
        }

        private void ReadData()
        {
            List<string> stockList = Config.GetInstance().GetStringValue(ConfigEnum.STOCK_TO_VIEW).Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries).ToList();
            
            _stockGridView.Rows.Clear();

            foreach (var ea in stockList)
            {
                string[] stockDetail = ea.Split(Config.FIELD_SEPARATOR);
                _stockGridView.Rows.Add(stockDetail[0], stockDetail[1], stockDetail[2], stockDetail[3], stockDetail[4], stockDetail[5]);
            }
        }

        private void SaveBtnAction(object sender, EventArgs e)
        {
            List<string> usernameAndPasswordList = new List<string>();

            List<string> stockList = new List<string>();
            for (int rows = 0; rows < _stockGridView.Rows.Count - 1; rows++)
            {
                if (_stockGridView.Rows[rows].Cells[0].Value != null && _stockGridView.Rows[rows].Cells[1].Value != null)
                {
                    string username = _stockGridView.Rows[rows].Cells[0].Value.ToString();
                    string password = _stockGridView.Rows[rows].Cells[1].Value.ToString();
                    string stockBelongFavorite = _stockGridView.Rows[rows].Cells[2].Value.ToString();
                    string stockName = _stockGridView.Rows[rows].Cells[3].Value.ToString();

                    string value = username + Config.FIELD_SEPARATOR + password + Config.FIELD_SEPARATOR + stockBelongFavorite + Config.FIELD_SEPARATOR + stockName;
                    if (_stockGridView.Rows[rows].Cells[4].Value != null)
                    {
                        value += Config.FIELD_SEPARATOR + _stockGridView.Rows[rows].Cells[4].Value.ToString();
                    }
                    else
                    {
                        value += Config.FIELD_SEPARATOR + StockModel.DEFAULT_INTERVAL_IN_SECOND.ToString();
                    }

                    if (_stockGridView.Rows[rows].Cells[5].Value != null)
                    {
                        value += Config.FIELD_SEPARATOR + _stockGridView.Rows[rows].Cells[5].Value.ToString();
                    }
                    else
                    {
                        value += Config.FIELD_SEPARATOR + StockModel.DEFAULT_STEPPING_SIZE.ToString();
                    }

                    stockList.Add(value);
                }
            }

            
            Config.GetInstance().Update(ConfigEnum.STOCK_TO_VIEW.GetKey(), String.Join(",", stockList.ToList()));
            Config.GetInstance().Save();

            ReadData();
        }
    }
}
