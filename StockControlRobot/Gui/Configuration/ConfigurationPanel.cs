﻿using StockControlRobot.Database;
using System;
using System.Configuration;
using System.Drawing;
using System.Windows.Forms;

namespace StockControlRobot.Gui.Configuration
{
    public class ConfigurationPanel : Panel, IPanel
    {
        private FlowLayoutPanel _mainPanel = new FlowLayoutPanel();
        private FlowLayoutPanel _advanceSettingPanel = new FlowLayoutPanel();
        private FlowLayoutPanel _controlPanel = new FlowLayoutPanel();
        private CheckBox _autoModeOnStartup = new CheckBox();

        private GroupBox _settingGroupBox = new GroupBox();

        private TableLayoutPanel _webSettingPanel = new TableLayoutPanel();
        private Label _referenceWebsiteLabel = new Label();
        private TextBox _referenceWebsiteTxt = new TextBox();

        private TableLayoutPanel _collectIntervalSettingPanel = new TableLayoutPanel();
        private Label _collectIntervalLabel = new Label();
        private TextBox _collectIntervalTxt = new TextBox();

        private Button _saveBtn = new Button();

        public ConfigurationPanel()
        {
            Init();
        }

        private void Init()
        {
            BackColor = Color.White;
            Dock = DockStyle.Fill;

            _settingGroupBox.Dock = DockStyle.Fill;
            _settingGroupBox.Text = "System Setting";

            _autoModeOnStartup.Text = "Auto Mode On Startup";
            _autoModeOnStartup.TextAlign = ContentAlignment.MiddleCenter;

            _mainPanel.Dock = DockStyle.Fill;
            _mainPanel.Padding = new Padding(10);
            _mainPanel.FlowDirection = FlowDirection.TopDown;

            _controlPanel.Dock = DockStyle.Bottom;
            _controlPanel.Padding = new Padding(10);
            _controlPanel.FlowDirection = FlowDirection.LeftToRight;

            _advanceSettingPanel.Location = new Point(5,20);
            _advanceSettingPanel.AutoSize = true;
            _advanceSettingPanel.AutoSizeMode = AutoSizeMode.GrowOnly;
            _advanceSettingPanel.FlowDirection = FlowDirection.TopDown;
            _advanceSettingPanel.Controls.Add(_autoModeOnStartup);

            _webSettingPanel.ColumnCount = 2;
            _webSettingPanel.RowCount = 1;
            _webSettingPanel.AutoSize = true;
            _webSettingPanel.ColumnStyles.Add(new ColumnStyle(SizeType.AutoSize));
            _webSettingPanel.ColumnStyles.Add(new ColumnStyle(SizeType.AutoSize));
            _webSettingPanel.RowStyles.Add(new RowStyle(SizeType.AutoSize));

            _referenceWebsiteTxt.Width = 200;
            _referenceWebsiteLabel.Text = "Source Website ";
            _referenceWebsiteLabel.TextAlign = ContentAlignment.MiddleLeft;
            _webSettingPanel.Controls.Add(_referenceWebsiteLabel, 0, 0);
            _webSettingPanel.Controls.Add(_referenceWebsiteTxt, 1, 0);

            _collectIntervalSettingPanel.ColumnCount = 2;
            _collectIntervalSettingPanel.RowCount = 1;
            _collectIntervalSettingPanel.AutoSize = true;
            _collectIntervalSettingPanel.ColumnStyles.Add(new ColumnStyle(SizeType.AutoSize));
            _collectIntervalSettingPanel.ColumnStyles.Add(new ColumnStyle(SizeType.AutoSize));
            _collectIntervalSettingPanel.RowStyles.Add(new RowStyle(SizeType.AutoSize));

            _collectIntervalTxt.Width = 200;
            _collectIntervalLabel.Text = "Data Collect Interval In Second ";
            _collectIntervalLabel.TextAlign = ContentAlignment.MiddleLeft;
            _collectIntervalSettingPanel.Controls.Add(_collectIntervalLabel, 0, 0);
            _collectIntervalSettingPanel.Controls.Add(_collectIntervalTxt, 1, 0);

            _saveBtn.Click += SaveBtnAction;
            _saveBtn.Text = "Save Setting";

            _mainPanel.Controls.Add(_webSettingPanel);
            _mainPanel.Controls.Add(_collectIntervalSettingPanel);
            _mainPanel.Controls.Add(_advanceSettingPanel);
            _settingGroupBox.Controls.Add(_mainPanel);
            _controlPanel.Controls.Add(_saveBtn);

            Padding = new Padding(20);
            Controls.Add(_settingGroupBox);
            Controls.Add(_controlPanel);
        }

        public bool IsReadyToClose()
        {
            return true;
        }

        public void Start()
        {
            _referenceWebsiteTxt.Text = Config.GetInstance().GetStringValue(ConfigEnum.WEBSITE);
            _autoModeOnStartup.Checked = Config.GetInstance().GetBooleanValue(ConfigEnum.AUTO_MODE_ON_STARTUP);
            _collectIntervalTxt.Text = (Config.GetInstance().GetIntegerValue(ConfigEnum.DATA_COLLECT_INTERVAL_IN_MS) / 1000).ToString();
        }

        public void Stop()
        {
            //do nothing
        }

        private void SaveBtnAction(object sender, EventArgs e)
        {
            Config.GetInstance().Update(ConfigEnum.AUTO_MODE_ON_STARTUP.GetKey(), _autoModeOnStartup.Checked ? "true" : "false");
            Config.GetInstance().Update(ConfigEnum.WEBSITE.GetKey(), _referenceWebsiteTxt.Text);
            Config.GetInstance().Update(ConfigEnum.DATA_COLLECT_INTERVAL_IN_MS.GetKey(), (int.Parse(_collectIntervalTxt.Text) * 1000).ToString());
            Config.GetInstance().Save();
        }
    }
}
