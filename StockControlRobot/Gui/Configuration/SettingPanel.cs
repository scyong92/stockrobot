﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace StockControlRobot.Gui.Configuration
{
    public class SettingPanel : Panel, IPanel
    {
        private MenuStrip _sideMenu = new MenuStrip();
        private ToolStripMenuItem _loginMenuItem = new ToolStripMenuItem();
        private ToolStripMenuItem _settingMenuItem = new ToolStripMenuItem();
        private IPanel _configurationPanel = new ConfigurationPanel();
        private IPanel _profilePanel = new ProfilePanel();
        private IPanel _currentPanel;
        private Dictionary<IPanel, ToolStripMenuItem> _panelToMenuDic = new Dictionary<IPanel, ToolStripMenuItem>();

        public SettingPanel()
        {
            _panelToMenuDic.Add(_profilePanel, _loginMenuItem);
            _panelToMenuDic.Add(_configurationPanel, _settingMenuItem);
            Init();
        }

        private void Init()
        {
            Dock = DockStyle.Fill;

            _loginMenuItem.Text = "Login";
            _loginMenuItem.Padding = new Padding(5, 15, 5, 15);
            _loginMenuItem.Click += new EventHandler((sender, e) => MenuClickEvent(sender, e, _profilePanel));
            _loginMenuItem.Dock = DockStyle.Fill;

            _settingMenuItem.Text = "Setting";
            _settingMenuItem.Padding = new Padding(5, 15, 5, 15);
            _settingMenuItem.Click += new EventHandler((sender, e) => MenuClickEvent(sender, e, _configurationPanel));
            _settingMenuItem.Dock = DockStyle.Fill;

            _sideMenu.Items.Add(_loginMenuItem);
            _sideMenu.Items.Add(_settingMenuItem);
            _sideMenu.Dock = DockStyle.Left;

            _sideMenu.Renderer = new TopMenuRenderer();
            Controls.Add((Panel)_profilePanel);
            Controls.Add((Panel)_configurationPanel);
            Controls.Add(_sideMenu);
            _currentPanel = _profilePanel;
        }

        public bool IsReadyToClose()
        {
            return ((IPanel)_currentPanel).IsReadyToClose();
        }

        public void Start()
        {
            SwitchScreen(_currentPanel);
        }

        public void Stop()
        {
            ((IPanel)_currentPanel).Stop();
        }

        private void MenuClickEvent(Object sender, EventArgs e, IPanel panel)
        {
            SwitchScreen(panel);
        }

        public void SwitchScreen(IPanel screen)
        {

            if (_currentPanel != null)
            {
                IPanel currentPanel = (IPanel)_currentPanel;

                if (!currentPanel.IsReadyToClose())
                {
                    return;
                }

                currentPanel.Stop();
                ((Panel)currentPanel).Hide();
                _panelToMenuDic[currentPanel].Enabled = true;
            }

            _currentPanel = screen;

            screen.Start();
            ((Panel)screen).Show();
            _panelToMenuDic[screen].Enabled = false;
        }
    }
}
