﻿using System.Drawing;
using System.Windows.Forms;

namespace StockControlRobot.Gui
{
    public class TopMenuRenderer : ToolStripProfessionalRenderer
    {
        protected override void OnRenderMenuItemBackground(ToolStripItemRenderEventArgs e)
        {
            if (!((ToolStripMenuItem)e.Item).Enabled)
            {
                Rectangle rc = new Rectangle(Point.Empty, e.Item.Size);
                base.OnRenderMenuItemBackground(e);
                using (SolidBrush brush = new SolidBrush(Color.AliceBlue))
                {
                    e.Graphics.FillRectangle(brush, rc);
                }
            }
        }
    }
}
