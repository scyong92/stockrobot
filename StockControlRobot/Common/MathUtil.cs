﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace StockControlRobot.Common
{
    public class MathUtil
    {
        private const int MIN_OUTLIER_REMOVAL_DATASET_SIZE_REQUIRED = 5;

        public static void RemoveOutlier(List<decimal> dataSet)
        {
            if (dataSet.Count() >= MIN_OUTLIER_REMOVAL_DATASET_SIZE_REQUIRED)
            {
                decimal interquartileRangeFactor = 1.5m;

                dataSet.Sort();
                int dataSetLength = dataSet.Count();

                int firstQuartileIndex = int.Parse(Math.Floor((dataSetLength + 1) * 0.25m).ToString());
                int thirdQuartileIndex = int.Parse(Math.Floor((dataSetLength + 1) * 0.75m).ToString());

                decimal interquartileRange = dataSet[thirdQuartileIndex] - dataSet[firstQuartileIndex];

                decimal lowerLimit = dataSet[thirdQuartileIndex] - interquartileRange * interquartileRangeFactor;
                decimal upperLimit = dataSet[thirdQuartileIndex] + interquartileRange * interquartileRangeFactor;

                dataSet = dataSet.Where(i => i >= lowerLimit && i <= upperLimit).ToList();
            }
        }
    }
}
