﻿using System;
using System.Collections.Generic;

namespace StockControlRobot.Common
{
    public class Enum : IComparable
    {
        private int _id;
        private int _hashCode;
        private static Dictionary<Type, Dictionary<int, Enum>> _enumClassToIdToInstanceMapMap = new Dictionary<Type, Dictionary<int, Enum>>();

        protected Enum(int id)
        {
            _id = id;
            _hashCode = 37 * GetType().GetHashCode() + id;

            Dictionary<int, Enum> instanceMap = new Dictionary<int, Enum>();
            if (!_enumClassToIdToInstanceMapMap.ContainsKey(GetType()))
            {
                instanceMap = new Dictionary<int, Enum>();
                _enumClassToIdToInstanceMapMap.Add(GetType(), instanceMap);
            }
            else
            {
                instanceMap = _enumClassToIdToInstanceMapMap[GetType()];
            }
            instanceMap.Add(id, this);
        }

        public override int GetHashCode()
        {
            return _hashCode;
        }

        public override bool Equals(Object rhs)
        {
            if (rhs == null)
            {
                return false;
            }

            if (rhs == this)
            {
                return true;
            }

            if (GetType() == rhs.GetType())
            {
                Enum enumeration = (Enum)rhs;
                if (_id == enumeration._id)
                {
                    return true;
                }
            }

            return false;
        }

        public int GetId()
        {
            return _id;
        }

        public int CompareTo(object obj)
        {
            Enum enumeration = (Enum)obj;
            if (_id > enumeration._id)
            {
                return 1;
            }

            if (_id < enumeration._id)
            {
                return -1;
            }

            return 0;
        }
    }
}
