﻿using StockControlRobot.Database;
using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;

namespace StockControlRobot.Common
{
    public class Util
    {
        private const string ENCRYPTION_KEY = "StockControlRobot";

        public static string Encrypt(string input)
        {
            using (var md5 = new MD5CryptoServiceProvider())
            {
                using (var tdes = new TripleDESCryptoServiceProvider())
                {
                    tdes.Key = md5.ComputeHash(UTF8Encoding.UTF8.GetBytes(ENCRYPTION_KEY));
                    tdes.Mode = CipherMode.ECB;
                    tdes.Padding = PaddingMode.PKCS7;

                    using (var transform = tdes.CreateEncryptor())
                    {
                        byte[] textBytes = UTF8Encoding.UTF8.GetBytes(input);
                        byte[] bytes = transform.TransformFinalBlock(textBytes, 0, textBytes.Length);
                        return Convert.ToBase64String(bytes, 0, bytes.Length);
                    }
                }
            }
        }

        public static string Decrypt(string cipherText)
        {
            using (var md5 = new MD5CryptoServiceProvider())
            {
                using (var tdes = new TripleDESCryptoServiceProvider())
                {
                    tdes.Key = md5.ComputeHash(UTF8Encoding.UTF8.GetBytes(ENCRYPTION_KEY));
                    tdes.Mode = CipherMode.ECB;
                    tdes.Padding = PaddingMode.PKCS7;

                    using (var transform = tdes.CreateDecryptor())
                    {
                        byte[] cipherBytes = Convert.FromBase64String(cipherText);
                        byte[] bytes = transform.TransformFinalBlock(cipherBytes, 0, cipherBytes.Length);
                        return UTF8Encoding.UTF8.GetString(bytes);
                    }
                }
            }
        }

        public static void WriteLog(string message)
        {
            string defaultFileDir = Config.GetInstance().GetStringValue(ConfigEnum.LOG_FILE_DIR);
            Directory.CreateDirectory(defaultFileDir);

            StringBuilder sb = new StringBuilder();
            sb.Append(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "\r\n--------------------------------\r\n" + message + "\r\n\r\n");
            File.AppendAllText(defaultFileDir + "exception.txt", sb.ToString());
            sb.Clear();
        }
    }
}
