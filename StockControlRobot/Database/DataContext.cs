﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SQLite;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;

namespace StockControlRobot.Database
{
    class DataContext
    {
        private string _connectionString;
        private string _databasePath;
        private string _databaseName;

        private static DataContext _instance;

        [MethodImpl(MethodImplOptions.Synchronized)]
        public static DataContext GetInstance()
        {
            if (_instance == null)
            {
                _instance = new DataContext();
            }
            return _instance;
        }

        public DataContext()
        {
            _databasePath = Config.GetInstance().GetStringValue(ConfigEnum.DATABASE_RELATIVE_PATH);
            _databaseName = Config.GetInstance().GetStringValue(ConfigEnum.DATABASE_FILE_NAME);
            _connectionString = $"Data Source={Path.Combine(_databasePath, _databaseName)};";
        }

        public DataTable ExecuteQuery(string sqlCommand)
        {
            DataTable dt = new DataTable();
            using (var conn = new SQLiteConnection(_connectionString))
            {
                using (var da = new SQLiteDataAdapter(sqlCommand, conn))
                {
                    da.Fill(dt);
                }
            }

            return dt;
        }

        public bool ExecuteNonQuery(string sqlCommand)
        {
            int lineAffected = 0;

            using (var conn = new SQLiteConnection(_connectionString))
            {
                conn.Open();
                var command = new SQLiteCommand(sqlCommand, conn);
                lineAffected = command.ExecuteNonQuery();
            }

            return lineAffected != 0;
        }

        public Object ExecuteScalarQuery(string sqlCommand)
        {
            Object returnData = null;

            using (var conn = new SQLiteConnection(_connectionString))
            {
                conn.Open();
                var command = new SQLiteCommand(sqlCommand, conn);
                returnData = command.ExecuteScalar();
            }

            return returnData;
        }

        public void Init()
        {
            string databaseFullPath = Path.Combine(_databasePath, _databaseName);

            if (!File.Exists(databaseFullPath))
            {
                Directory.CreateDirectory(_databasePath);
                SQLiteConnection.CreateFile(databaseFullPath);
            }

            string stockTableCreationQuery = @"CREATE TABLE IF NOT EXISTS [Stock] (
                                                    [Id] INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
                                                    StockCode NVARCHAR(255) not null,
                                                    StepSize int not null default 2,
                                                    Interval int not null default 300,
                                                    ResistenceLine decimal(18,6),
                                                    SupportLine decimal(18,6),
                                                    CreatedDate date not null default CURRENT_DATE,
                                                    CONSTRAINT UniqueStockDate UNIQUE (StockCode, CreatedDate))";

            string stockDataTableCreationQuery = @"CREATE TABLE IF NOT EXISTS [StockData] (
                                                    [Id] INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
                                                    StockId int not null,
                                                    BuyValue decimal(18,6) not null default 0,
                                                    CreatedDate datetime not null default CURRENT_TIMESTAMP,
                                                    CONSTRAINT FK_StockData_StockId FOREIGN KEY (StockId) REFERENCES Stock(Id))";

            ExecuteNonQuery(stockTableCreationQuery);
            ExecuteNonQuery(stockDataTableCreationQuery);
        }
    }
}
