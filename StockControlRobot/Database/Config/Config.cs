﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Runtime.CompilerServices;
using System.Linq;

namespace StockControlRobot.Database
{
    public class Config
    {
        private static Config _instance;
        public const char FIELD_SEPARATOR = '-';
        private Configuration _appConfig = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);

        [MethodImpl(MethodImplOptions.Synchronized)]
        public static Config GetInstance()
        {
            if (_instance == null)
            {
                _instance = new Config();
            }

            return _instance;
        }

        public string GetStringValue(ConfigEnum configEnum)
        {
            return _appConfig.AppSettings.Settings[configEnum.GetKey()].Value.ToString();
        }

        public int GetIntegerValue(ConfigEnum configEnum)
        {
            string value = GetStringValue(configEnum);
            return string.IsNullOrEmpty(value) ? 0 : int.Parse(value);
        }

        public bool GetBooleanValue(ConfigEnum configEnum)
        {
            string value = GetStringValue(configEnum);
            return string.IsNullOrEmpty(value) ? false : Boolean.Parse(value);
        }

        public void Save()
        {
            _appConfig.Save();
            ConfigurationManager.RefreshSection("appSettings");
        }
        
        public void Update(string key, string value)
        {
            _appConfig.AppSettings.Settings[key].Value = value;
        }

        public bool IsStockConfigured()
        {
            return Config.GetInstance().GetStringValue(ConfigEnum.STOCK_TO_VIEW).Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries).ToList().Count > 0;
        }
    }
}
