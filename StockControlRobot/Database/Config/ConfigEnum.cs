﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace StockControlRobot.Database
{
    public class ConfigEnum : Common.Enum
    {
        private static int _index = -1;
        public static Dictionary<string, ConfigEnum> _configMap = new Dictionary<string, ConfigEnum>();

        public static ConfigEnum DATABASE_RELATIVE_PATH = new ConfigEnum(++_index, "databaseRelativePath", "db\\");
        public static ConfigEnum DATABASE_FILE_NAME = new ConfigEnum(++_index, "databaseFileName", "database.sqlite");
        public static ConfigEnum LOG_FILE_DIR = new ConfigEnum(++_index, "logFileDir", "log\\");
        public static ConfigEnum WEBSITE = new ConfigEnum(++_index, "website", "https://www.kentrade.com.my/");
        public static ConfigEnum STOCK_TO_VIEW = new ConfigEnum(++_index, "stockToView", "");
        public static ConfigEnum DATA_COLLECT_INTERVAL_IN_MS = new ConfigEnum(++_index, "dataCollectInSecond", "5000");
        public static ConfigEnum MANUAL_MODE = new ConfigEnum(++_index, "manualMode", "false");
        public static ConfigEnum AUTO_MODE_ON_STARTUP = new ConfigEnum(++_index, "autoModeOnStartup", "false");

        private object _value;
        private string _configKey;

        public ConfigEnum(int index, string configKey, object defaultValue)
            : base(index)
        {
            _configKey = configKey;
            _value = defaultValue;
            _configMap.Add(configKey, this);
        }

        public static List<ConfigEnum> GetAllConfigKey()
        {
            return _configMap.Values.ToList();
        }

        public static ConfigEnum GetConfigByString(string key)
        {
            return _configMap[key];
        }

        public object GetDefaultValue()
        {
            return _value;
        }

        public string GetKey()
        {
            return _configKey;
        }
    }
}
